#include <QCoreApplication>
#include "RenderingSimulation.h"
#include <iostream>
#include "QThread"
#define VOL_PREFIX "/projects/volume-datasets/foot/foot"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Volume< uchar >* volume = new Volume< uchar >(VOL_PREFIX);
    RenderingSimulation *rs = new RenderingSimulation(volume);
    rs->startSimulation();
    return a.exec();
}
