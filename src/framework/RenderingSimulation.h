#ifndef RENDERINGSIMULATION_H
#define RENDERINGSIMULATION_H

#include "ParallelRendering.h"
#include <algorithm>
#include <math.h>

typedef std::unordered_map< CLAbstractRenderer* , QVector< double >* > ProfileAcumulator;
typedef std::unordered_map< CLAbstractRenderer* , uint > GPUsRanks;

class RenderingSimulation :public ParallelRendering
{
    Q_OBJECT

public:

    /**
     * @brief RenderingSimulation
     */
    RenderingSimulation(Volume< uchar >* volume ,
                        const uint frameWidth = 2048,
                        const uint frameHeight = 2048);

    /**
     * @brief initRenderers
     */
    void addCLRenderer(const uint64_t gpuIndex);

    /**
     * @brief distributeBaseVolume1D
     */
    void distributeBaseVolume1D();

    void startSimulation();

    /**
     * @brief getGPUsRanks
     * @return
     */
    GPUsRanks getGPUsRanks();

    /**
     * @brief getGPUsMeanExecutionTime
     * @return
     */
    GPUsRanks getGPUsMeanExecutionTime();

    /**
     * @brief getGpusScores
     * @return
     */
    QVector<uint > getGpusScores();

signals:

    /**
     * @brief finishedSimulation
     */
    void finishedSimulation();

protected:

    /**
     * @brief applyTransformation_
     */
    void applyTransformation_();

private:

    /**
     * @brief runRendringLoop_
     */
    void runRendringLoop_();

    /**
     * @brief rankRendeingNodes_
     */
    void rankRendeingNodes_();

public slots:

    /**
     * @brief renderingFinished_SLOT
     * @param renderer
     */
    void renderingFinished_SLOT(CLAbstractRenderer* renderer);

private:

    /**
     * @brief profilingAcc_
     */
    ProfileAcumulator profilingAcc_;

    /**
     * @brief meanTime_
     */
    GPUsRanks meanFrameRates_;

    /**
     * @brief gpuScores_
     */
    QVector< uint > gpuScores_;

};

#endif // RENDERINGSIMULATION_H
