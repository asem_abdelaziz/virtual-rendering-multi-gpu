#include "CLAbstractCompositor.h"


CLAbstractCompositor::CLAbstractCompositor( const uint64_t gpuIndex )
    : gpuIndex_( gpuIndex )
{
    readOutReady_ = false ;
    initializeContext_( );
}

uint64_t CLAbstractCompositor::getGPUIndex() const
{
    return gpuIndex_ ;
}

bool CLAbstractCompositor::readOutReady() const
{
    return readOutReady_ ;
}

void CLAbstractCompositor::selectGPU_()
{
    // Scan the hardware
    oclHWDL::Hardware* clHardware = new oclHWDL::Hardware( );

    // Get a list of all the GPUs that are connected to the system
    const oclHWDL::Devices listGPUs = clHardware->getListGPUs( );

    // Select the GPU that will be used for running the kernel
    oclHWDL::Device* selectedGPU = listGPUs.at( gpuIndex_ );
    device_ = selectedGPU->getId();

    // Get the platform that corresponds to the GPU
    platform_ = selectedGPU->getPlatform()->getPlatformId();

    // Create the OpenCL context
    oclHWDL::Context* clContext =
            new oclHWDL::Context(selectedGPU, oclHWDL::BASIC_CL_CONTEXT );
    context_ = clContext->getContext( );
}

void CLAbstractCompositor::initializeContext_()
{
    LOG_DEBUG( "Initializing an OpenCL context ... " );

    selectGPU_( );
    createCommandQueue_( );

    LOG_DEBUG( "[DONE] Initializing an OpenCL context ... " );
}

void CLAbstractCompositor::createCommandQueue_()
{
    cl_int clErrorCode = CL_SUCCESS;
    commandQueue_ = clCreateCommandQueue( context_ ,
                                          device_ ,
                                          0, &clErrorCode );

    if( clErrorCode != CL_SUCCESS )
        oclHWDL::Error::checkCLError( clErrorCode );
}

