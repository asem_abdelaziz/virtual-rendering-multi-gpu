#include "RenderingSimulation.h"

#define LOOP_ITERATIONS 10


RenderingSimulation::RenderingSimulation(Volume<uchar> *volume, const uint frameWidth,
                                         const uint frameHeight)
    :ParallelRendering( volume , frameWidth , frameHeight )

{
    LOG_DEBUG("Rendering Simulation Created");

    //Deploy all GPUs
    discoverAllNodes();

    //Distribute the same volume for all
    distributeBaseVolume1D();
}

void RenderingSimulation::addCLRenderer(const uint64_t gpuIndex)
{
    LOG_DEBUG("Adding CLRenderer");

    // if device already occupied by a rendering node, return.
    if( inUseGPUs_.contains( listGPUs_.at( gpuIndex ))) return;

    // pass transformations by reference.
    // translationAsync_, rotationAsync_, volumeDensityAsync_, brightnessAsync_
    // will be accessed by multithreads. So,
    // they should not be modified during the life of the threads.
    CLAbstractRenderer *renderer
            = ( CLAbstractRenderer* )
            new CLRenderer< uchar , float >( gpuIndex,
                                             frameWidth_ ,
                                             frameHeight_ ,
                                             transformationAsync_ );

    QVector<double> * accumulator = new QVector<double>();

    auto attachedGPU = listGPUs_.at( gpuIndex );

    // Add the recently attached GPU to Set of usedGPUs.
    inUseGPUs_ << attachedGPU;

    // Add the rendering node to the map< gpu , rendering node >.
    renderers_[ attachedGPU ] = renderer;

    //Add the rendring node to the map< rendering node , accumulator >.
    profilingAcc_[renderer] = accumulator ;

    // Create the TaskRender that wrap the rendering instruction,
    // to be executed concurrently on each device.
    TaskRender *taskRender = new TaskRender( *renderer  );

    // Add the new task object to
    // the map < rendering node , corresponding rendering task >
    renderingTasks_[ renderer ] = taskRender;
    rendererPool_.setMaxThreadCount( inUseGPUs_.size( ));

    connect( renderer , SIGNAL( finishedRendering( CLAbstractRenderer* )),
             this , SLOT( renderingFinished_SLOT( CLAbstractRenderer* )));

}

void RenderingSimulation::distributeBaseVolume1D()
{
    const int nDevices = inUseGPUs_.size();

    if( nDevices == 0 )
        LOG_ERROR( "No deployed devices to distribute volume!");

    //All GPUs will have the same dummy volume
    //Keeping Same structure of data types
    const int partitions = 1 ;

    QVector< Volume8 *> bricks = baseVolume_->heuristicBricking( partitions );

    for( auto renderingDevice  : inUseGPUs_ )
    {
        LOG_DEBUG( "Loading subVolume to device" );

        //Only one partition So only one entry
        auto subVolume = bricks[ 0 ];
        VolumeVariant volume = VolumeVariant::fromValue( subVolume );

        renderers_[ renderingDevice ]->loadVolume( volume );

        LOG_DEBUG( "[DONE] Loading subVolume to GPU <%d>",
                   renderers_[ renderingDevice ]->getGPUIndex( ));
    }

}

void RenderingSimulation::startSimulation()
{
    LOG_DEBUG("Runing Simulator");
    startRendering();
}

void RenderingSimulation::applyTransformation_()
{
    // fetch new transformations if exists.
    syncTransformation_();

    for( const oclHWDL::Device *renderingDevice : inUseGPUs_ )
    {
        const CLAbstractRenderer *renderer = renderers_.at( renderingDevice );

        // Spawn threads and start rendering on each rendering node.
        rendererPool_.start( renderingTasks_.at( renderer ));
    }

    pendingTransformations_ = false;
    renderersReady_ = false;
}

QVector<uint> RenderingSimulation::getGpusScores()
{
    return gpuScores_;
}


void RenderingSimulation::runRendringLoop_()
{
    if(!renderersReady_)
        return;

    static int i = 0 ;
    if(++i  < LOOP_ITERATIONS)
    {
        //Update rotation
        transformation_.rotation.x++;
        applyTransformation_();
    }
    else
        rankRendeingNodes_();

}

void RenderingSimulation::rankRendeingNodes_()
{
    for( auto it : profilingAcc_ )
    {
        QVector<double> * accumulator = new QVector<double>();
        accumulator = it.second;

        //calculate the average execution time for each GPU in nano seconds
        double mean = std::accumulate(accumulator->begin(),
                                      accumulator->end(), 0.0) / accumulator->size();

        //Get the Frame Rate in frames/sec
        meanFrameRates_[it.first] =10e9/mean;

        LOG_DEBUG("For GPU<%d> mean: %d  frame/sec",it.first->getGPUIndex(),
                  meanFrameRates_[it.first]);
    }

    for(auto i : meanFrameRates_)
    {
        uint frameRate = i.second;

        //Update GPUs Scores vector
        gpuScores_.append(frameRate);        
    }

    emit finishedSimulation();
}

void RenderingSimulation::renderingFinished_SLOT(CLAbstractRenderer *renderer)
{

    static int i = 0;

    //Update Accumulator
    profilingAcc_[renderer]->append(renderer->getRenderingTime());

    if(++i == inUseGPUs_.size())
    {
        renderersReady_ = true ;
        i = 0 ;
    }
    runRendringLoop_();
}


